package com.pratik.appedia.androidtutorials_snackbar;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button button_One, button_Two, button_Three;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        button_One = findViewById(R.id.button_One);
        button_Two = findViewById(R.id.button_Two);
        button_Three = findViewById(R.id.button_Three);

        button_One.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDefaultSnackbar();
            }
        });

        button_Two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showActionSnackbar();
            }
        });

        button_Three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomSnackbar();
            }
        });
    }

    private void showCustomSnackbar() {
        Snackbar snackbar = Snackbar.make(coordinatorLayout,"Failed to send message...",Snackbar.LENGTH_SHORT);
        snackbar.setAction("RETRY", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        TextView snackbarTextView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        snackbarTextView.setTextColor(Color.YELLOW);
        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/Lobster.otf");
        snackbarTextView.setTypeface(typeface);

        snackbar.addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                Toast.makeText(getApplicationContext(),"Snackbar dismissed",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onShown(Snackbar transientBottomBar) {
                super.onShown(transientBottomBar);
                Toast.makeText(getApplicationContext(),"Snackbar shown",Toast.LENGTH_SHORT).show();
            }
        });
        snackbar.show();
    }

    private void showActionSnackbar() {
        Snackbar snackbar = Snackbar.make(coordinatorLayout,"Message has been sent...",Snackbar.LENGTH_SHORT);
        snackbar.setAction("UNDO", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar1 = Snackbar.make(coordinatorLayout,"Message has been revoked!",Snackbar.LENGTH_SHORT);
                snackbar1.show();
            }
        });
        snackbar.show();
    }

    private void showDefaultSnackbar() {
        Snackbar snackbar = Snackbar.make(coordinatorLayout,"Default snackbar being shown",Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
}
